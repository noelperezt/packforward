var flag = false;
jQuery(document).ready(function ($) {
    if($('#navbar_principal').css('display') == 'block' && $('#navbar_secundario').css('display') == 'none' ){
        $('.contenedor-user').css('right', '124px', 'important');
    }
    if($('#navbar_secundario').css('display') == 'block' && $('#navbar_principal').css('display') == 'none'){
        $('.contenedor-user').css('right', '30px', 'important');
    }
//MENU MOVIL
    /* Enseñar login del menu movil */
    $('#iniciar_movil').click(function () {
        if(!flag){
            $('#login').show();
            flag = true;
        }            
        else{
            $('#login').hide();
            flag = false;
        }
    });
    /*$('.wrapper').mouseleave(function () {
        $('#login').hide();
    });
    /*Previa de la caja */
    $('.caja').mouseover(function () {
        $('.previa').show();
    });
    $('.wrapper').mouseleave(function () {
        $('.previa').hide();
    });
    /*Previa del Usuario */
    $('.usuario_logeado').mouseover(function () {
        $('.contenedor-user').show();
    });
    $('.wrapper').mouseleave(function () {
        $('.contenedor-user').hide();
    });
    /*Quitar item de la tabla previa de la caja*/
    $("#tabla").on('click', '.glyphicon-remove', function () {
        $(this).closest('tr').remove();
    });
//MENU SECUNDARIO
    /* Enseñar login del menu secundario */
    /*
    $('#iniciar_dos').click(function () {
        $('#login').show();
    });
    $('.wrapper').mouseleave(function () {
        $('#login').hide();
    });
    /*Previa de la caja */
    /*
    $('.caja').mouseover(function () {
        $('.previa').show();
    });
    $('.wrapper').mouseleave(function () {
        $('.previa').hide();
    });
    /*Previa del Usuario */
    /*
    $('.usuario_logeado').mouseover(function () {
        $('.contenedor-user').show();
    });
    $('.wrapper').mouseleave(function () {
        $('.contenedor-user').hide();
    });
    /*Quitar item de la tabla previa de la caja*/
    /*
    $("#tabla").on('click', '.glyphicon-remove', function () {
        $(this).closest('tr').remove();
    });
//MENU PRINCIPAL
    /* Enseñar login del menu principal */
    $('#iniciar').click(function () {
        if(!flag){
            $('#login').show();
            flag = true;
        }            
        else{
            $('#login').hide();
            flag = false;
        }
    });
    /*$('.wrapper').mouseleave(function () {
        $('#login').hide();
    });
    /*Previa de la caja */
    $('.caja').mouseover(function () {
        $('.previa').show();
        $('.contenedor-user').hide();
    });
    $('.wrapper').mouseleave(function () {
        $('.previa').hide();
    });
    /*Previa del Usuario */
    $('.usuario_logeado').mouseover(function () {
        $('.contenedor-user').show();
        $('.previa').hide();
    });
    /*$('.wrapper').mouseleave(function () {
        $('.contenedor-user').hide();
    });
    /*Quitar item de la tabla previa de la caja*/
    $("#tabla").on('click', '.glyphicon-remove', function () {
        $(this).closest('tr').remove();
    });
});