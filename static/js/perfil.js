jQuery(document).ready(function ($) {
    $(function () {
        $('#browse').click(function () {
            $('#browse').hide();
            $('#submitfoto').show();
            $('#cancelar').show();
            $(this).parent().find('input').click();

        });

        $('#cancelar').click(function () {
            $('#browse').show();
            $('#submitfoto').hide();
            $('#cancelar').hide();
        });

        $('#editar').click(function () {
            document.getElementById("nombre").readOnly = false;
            document.getElementById("apellido").readOnly = false;
            document.getElementById("email").readOnly = false;
            document.getElementById("telefono").readOnly = false;
            document.getElementById("fax").readOnly = false;
            this.style.display = 'none';
            document.getElementById('submitperfil').style.display = 'inline-block';
        });

        $("#submitperfil").click(function () {
            if (validarCampos()) {
                $.ajax({
                    url: '/editarperfil/' + $("#id").val(),
                    type: 'POST',
                    data: {
                        nombre: $("#nombre").val(),
                        apellido: $("#apellido").val(),
                        correo: $("#email").val(),
                        telefono: $("#telefono").val(),
                        fax: $("#fax").val(),
                    },
                    dataType: 'json',
                    success: function (msj) {
                        document.getElementById("nombre").readOnly = true;
                        document.getElementById("apellido").readOnly = true;
                        document.getElementById("email").readOnly = true;
                        document.getElementById("telefono").readOnly = true;
                        document.getElementById("fax").readOnly = true;
                        document.getElementById('editar').style.display = 'inline-block';
                        document.getElementById('submitperfil').style.display = 'none';
                        //alert(msj.mensaje)
                        bootbox.alert({
                            message: msj.mensaje,
                            size: 'small'
                        });


                    }
                });
            }
        });
    });
});

function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode;
    return ((key >= 48 && key <= 57) || e.keyCode == 9 || e.keyCode == 46)
}

//cargar imagen
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function validarCampos() {
    var error = 0;
    if ($('#nombre').val() == "") {
        $('#error_nombre').css('display', 'block');
        error = 1;
    } else {
        $('#error_nombre').css('display', 'none');
    }
    if ($('#apellido').val() == "") {
        $('#error_apellido').css('display', 'block');
        error = 1;
    } else {
        $('#error_apellido').css('display', 'none');
    }
    if ($('#email').val() == "") {
        $('#error_correo').css('display', 'block');
        error = 1;
    } else {
        $('#error_correo').css('display', 'none');
    }
    if ($('#telefono').val() == "") {
        $('#error_telefono').css('display', 'block');
        error = 1;
    } else {
        $('#error_telefono').css('display', 'none');
    }
    if ($('#fax').val() == "") {
        $('#error_fax').css('display', 'block');
        error = 1;
    } else {
        $('#error_fax').css('display', 'none');
    }
    if (error == 0) {
        return true;
    } else {
        return false;
    }
}

function validarCamposFac() {
    var error = 0;
    if ($('#pais_factura').val() == "") {
        $('#error_paisf').css('display', 'block');
        error = 1;
    } else {
        $('#error_paisf').css('display', 'none');
    }
    if ($('#estado_factura').val() == "") {
        $('#error_estadof').css('display', 'block');
        error = 1;
    } else {
        $('#error_estadof').css('display', 'none');
    }
    if ($('#ciudad_factura').val() == "") {
        $('#error_ciudadf').css('display', 'block');
        error = 1;
    } else {
        $('#error_ciudadf').css('display', 'none');
    }
    if ($('#postal_factura').val() == "") {
        $('#error_postalf').css('display', 'block');
        error = 1;
    } else {
        $('#error_postalf').css('display', 'none');
    }
    if ($('#calle_factura').val() == "") {
        $('#error_callef').css('display', 'block');
        error = 1;
    } else {
        $('#error_callef').css('display', 'none');
    }
    if ($('#telefono_factura').val() == "") {
        $('#error_telef').css('display', 'block');
        error = 1;
    } else {
        $('#error_telef').css('display', 'none');
    }
    if ($('#telefono2_factura').val() == "") {
        $('#error_telef2').css('display', 'block');
        error = 1;
    } else {
        $('#error_telef2').css('display', 'none');
    }
    if (error == 0) {
        return true;
    } else {
        return false;
    }
}

function validarCamposEnv() {
    var error = 0;
    if ($('#pais_envio').val() == "") {
        $('#error_paise').css('display', 'block');
        error = 1;
    } else {
        $('#error_paise').css('display', 'none');
    }
    if ($('#estado_envio').val() == "") {
        $('#error_estadoe').css('display', 'block');
        error = 1;
    } else {
        $('#error_estadoe').css('display', 'none');
    }
    if ($('#ciudad_envio').val() == "") {
        $('#error_ciudade').css('display', 'block');
        error = 1;
    } else {
        $('#error_ciudade').css('display', 'none');
    }
    if ($('#postal_envio').val() == "") {
        $('#error_postale').css('display', 'block');
        error = 1;
    } else {
        $('#error_postale').css('display', 'none');
    }
    if ($('#calle_envio').val() == "") {
        $('#error_callee').css('display', 'block');
        error = 1;
    } else {
        $('#error_callee').css('display', 'none');
    }
    if ($('#telefono_envio').val() == "") {
        $('#error_telee').css('display', 'block');
        error = 1;
    } else {
        $('#error_telee').css('display', 'none');
    }
    if ($('#telefono2_envio').val() == "") {
        $('#error_telee2').css('display', 'block');
        error = 1;
    } else {
        $('#error_telee2').css('display', 'none');
    }
    if (error == 0) {
        return true;
    } else {
        return false;
    }
}

function validarClave() {
    var error = 0;
    if ($("#password1").val() == "" || $("#password2").val() == "") {
        $('#error_pass1').css('display', 'block');
        error = 1;
    } else if ($("#password1").val() != $("#password2").val()) {
        $('#error_pass2').css('display', 'block');
        error = 1;
    } else {
        $('#error_recontra').css('display', 'none');
    }
    if (error == 0) {
        return true;
    } else {
        return false;
    }
}

function MostrarFormFac(id, p, e, c, ca, po, t, t2) {
    $('#id_dir').val(id);
    $('#pais_factura').val(p);
    $('#estado_factura').val(e);
    $('#ciudad_factura').val(c);
    $('#calle_factura').val(ca);
    $('#postal_factura').val(po);
    $('#telefono_factura').val(t);
    $('#telefono2_factura').val(t2);
    $('#formfac').modal('show');
}
function MostrarFormEnv(id, p, e, c, ca, po, t, t2) {
    $('#id_dir').val(id);
    $('#pais_envio').val(p);
    $('#estado_envio').val(e);
    $('#ciudad_envio').val(c);
    $('#calle_envio').val(ca);
    $('#postal_envio').val(po);
    $('#telefono_envio').val(t);
    $('#telefono2_envio').val(t2);
    $('#formenv').modal('show');
}

function MostrarFormClave() {
    $('#formclave').modal('show');
}
function MostrarFormPerfil() {
    $('#formperfil').show();
    $('#formclave').hide();
}

function EditarDirFac() {
    if ($("#id_dir").val() == 0) {
        NuevaDirFac();
    } else {
        if (validarCamposFac()) {
            $.ajax({
                url: '/editardirfac/' + $("#id_dir").val(),
                type: 'POST',
                data: {
                    pais: $("#pais_factura").val(),
                    estado: $("#estado_factura").val(),
                    ciudad: $("#ciudad_factura").val(),
                    calle: $("#calle_factura").val(),
                    codigo_postal: $("#postal_factura").val(),
                    telefono: $("#telefono_factura").val(),
                    telefono2: $("#telefono2_factura").val(),
                },
                dataType: 'json',
                success: function (msj) {
                    //alert(msj.mensaje)
                    bootbox.alert({
                        message: msj.mensaje,
                        size: 'small'
                    });
                }
            });
        }
    }
}

function EditarDirEnv() {
    if ($("#id_dir").val() == 0) {
        NuevaDirEnv();
    } else {
        if (validarCamposEnv()) {
            $.ajax({
                url: '/editardirenv/' + $("#id_dir").val(),
                type: 'POST',
                data: {
                    pais: $("#pais_envio").val(),
                    estado: $("#estado_envio").val(),
                    ciudad: $("#ciudad_envio").val(),
                    calle: $("#calle_envio").val(),
                    codigo_postal: $("#postal_envio").val(),
                    telefono: $("#telefono_envio").val(),
                    telefono2: $("#telefono2_envio").val(),
                },
                dataType: 'json',
                success: function (msj) {
                    //alert(msj.mensaje)
                    bootbox.alert({
                        message: msj.mensaje,
                        size: 'small'
                    });
                }
            });
        }
    }
}

function NuevaDirFac() {
    if (validarCamposFac()) {
        $.ajax({
            url: '/nuevadir/',
            type: 'POST',
            data: {
                usuario: $("#id").val(),
                pais: $("#pais_factura").val(),
                estado: $("#estado_factura").val(),
                ciudad: $("#ciudad_factura").val(),
                calle: $("#calle_factura").val(),
                codigo_postal: $("#postal_factura").val(),
                telefono: $("#telefono_factura").val(),
                telefono2: $("#telefono2_factura").val(),
                tipo: 1,
            },
            dataType: 'json',
            success: function (msj) {
                //alert(msj.mensaje)
                bootbox.alert({
                    message: msj.mensaje,
                    size: 'small'
                });
            }
        });
    }
}

function NuevaDirEnv() {
    if (validarCamposEnv()) {
        $.ajax({
            url: '/nuevadir/',
            type: 'POST',
            data: {
                usuario: $("#id").val(),
                pais: $("#pais_envio").val(),
                estado: $("#estado_envio").val(),
                ciudad: $("#ciudad_envio").val(),
                calle: $("#calle_envio").val(),
                codigo_postal: $("#postal_envio").val(),
                telefono: $("#telefono_envio").val(),
                telefono2: $("#telefono2_envio").val(),
                tipo: 2,
            },
            dataType: 'json',
            success: function (msj) {
                //alert(msj.mensaje)
                bootbox.alert({
                    message: msj.mensaje,
                    size: 'small'
                });
            }
        });
    }
}

function EditarClave() {
    if (validarClave()) {
        $.ajax({
            url: '/editarclave/' + $("#id").val(),
            type: 'POST',
            data: {
                clavenueva: $("#password2").val(),
            },
            dataType: 'json',
            success: function (msj) {
                //alert(msj.mensaje)
                bootbox.alert({
                    message: msj.mensaje,
                    size: 'small'
                });
            }
        });
    }
}