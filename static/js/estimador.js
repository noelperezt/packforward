function sum() 
{ 
    if( $('#aereo').attr("tab-pane", "active")) {
        var $peso_a, $largo_a, $alto_a, $ancho_a, $total_a;
        $peso_a = parseFloat(document.getElementById("peso_aereo").value, 10);
        $largo_a = parseFloat(document.getElementById("largo_aereo").value, 10);
        $alto_a = parseFloat(document.getElementById("alto_aereo").value, 10);
        $ancho_a = parseFloat(document.getElementById("ancho_aereo").value, 10);
        $total_a = ($peso_a + $largo_a + $alto_a + $ancho_a);
        document.getElementById("total_aereo").value = $total_a;
    }else{
        $('#maritimo').attr("tab-pane", "active")
            var $peso_m, $largo_m, $alto_m, $ancho_m, $total_m;
            $peso_m = parseFloat(document.getElementById("peso_mar").value, 10);
            $largo_m = parseFloat(document.getElementById("largo_mar").value, 10);
            $alto_m = parseFloat(document.getElementById("alto_mar").value, 10);
            $ancho_m = parseFloat(document.getElementById("ancho_mar").value, 10);
            $total_m = ($peso_m + $largo_m + $alto_m + $ancho_m);
            document.getElementById("total_mar").value = $total_m;
    }
}
