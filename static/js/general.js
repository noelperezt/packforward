jQuery(document).ready(function ($) {
    // INDEX
    $('#navbar_secundario').hide();
    //funcion scroll navbar
    $(window).scroll(function () {
        if (($(document).scrollTop() > 30)) {
            $('#navbar_principal').hide();
            $('#navbar_secundario').show();
        } else {
            $('#navbar_principal').show();
            $('#navbar_secundario').hide();
        }
    });
    $("#tabs").tab();
    $('#login').hide();
    //slider clientes
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 280,
        itemMargin: 8
    });
    //slider testimoniales
    $('.bxslider').bxSlider();

    //DETALLE PRODUCTO
    $('#detalles').click(function () {
        $('.envio').hide();
        $('.pago').hide();
        $('.detalles').show();
    })
    $('#pago').click(function () {
        $('.detalles').hide();
        $('.envio').hide();
        $('.pago').show();
    })
    $('#envio').click(function () {
        $('.detalles').hide();
        $('.pago').hide();
        $('.envio').show();
    })
    $('.slider1').bxSlider({
        slideWidth: 1190,
        minSlides: 4,
        maxSlides: 4,
        slideMargin: 5
    });
    $('.minis img').click(function () {
        var thmb = this;
        var src = this.src;
        $('.imagene_producto img').fadeOut(400, function () {
            thmb.src = this.src;
            $(this).fadeIn(400)[0].src = src;
        });
    });

    //INFORMACION
    var result = $(".tamaño-main").height();
    $('#info-main').css('height', result);
    $('#tamaño-second').css('height', result);

    $('.bx-wrapper').css('top', result / 5)

    //LISTADO
    $('.slider1').bxSlider({
        slideWidth: 1190,
        minSlides: 4,
        maxSlides: 4,
        slideMargin: 5
    });
    $('#icon_hrz').click(function () {
        $('.cuadricular').hide()
        $('.horizontal').show();
    });
    $('#icon_cuadros').click(function () {
        $('.cuadricular').show()
        $('.horizontal').hide();
    });
    $('.dropdown-submenu a.test').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    //NOSOTROS
    var result = $("#tamaño").height();
    $('#nosotros-info').css('height', result);

    //SERVICIOS
    $('#aereo-contenido').hide();
    //$('#serv-aereo').hide();
    $('#serv-aereo').click(function () {
        $(this).animate({left: '400px'}).hide();
    //$(this).hide();
    $('#aereo-contenido').show();

    });
});

//MAP OF INDEX
function myMap() {
    var mapOptions = {
        center: new google.maps.LatLng(36.2044029, -113.7621932, 4),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.HYBRID
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}