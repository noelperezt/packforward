jQuery(document).ready(function ($) {
    $('#checkSi').change(function () {
        $('#empresa').show();
    });
    $('#checkNo').change(function () {
        $('#empresa').hide();
    });
    $('#envioSi').change(function () {
        var error = 0;
        if ($("#id_pais").val() == "") {
            $('#error_pais').css('display', 'block');
            error = 1;
        } else {

            $('#error_pais').css('display', 'none');
        }
        if ($("#id_estado").val() == "") {
            $('#error_estado').css('display', 'block');
            error = 1;
        } else {

            $('#error_estado').css('display', 'none');
        }
        if ($("#id_ciudad").val() == "") {
            $('#error_ciudad').css('display', 'block');
            error = 1;
        } else {

            $('#error_ciudad').css('display', 'none');
        }
        if ($("#id_codigo_postal").val() == "") {
            $('#error_postal').css('display', 'block');
            error = 1;
        } else {

            $('#error_postal').css('display', 'none');
        }
        if ($("#id_calle").val() == "") {
            $('#error_calle').css('display', 'block');
            error = 1;
        } else {
            $('#error_calle').css('display', 'none');
        }
        if ($("#id_telefono").val() == "") {
            $('#error_telefact').css('display', 'block');
            error = 1;
        } else {
            $('#error_telefact').css('display', 'none');
        }
        if (error == 0) {
            $('.segundo').hide();
            $('#id_paise').val($("#id_pais").val());
            $('#id_estadoe').val($("#id_estado").val());
            $('#id_ciudade').val($("#id_ciudad").val());
            $('#id_codigo_postale').val($("#id_codigo_postal").val());
            $('#id_callee').val($("#id_calle").val());
            $('#id_telefonoe').val($("#id_telefono").val());
            $('#id_telefonoe2').val($("#id_telefono2").val());
            $('.tercero').show();
        }
    })
    $('#continuar').click(function () {
        var error = 0;
        if ($("#id_first_name").val() == "") {
            $('#error_nombre').css('display', 'block');
            error = 1;
        } else {

            $('#error_nombre').css('display', 'none');
        }
        if ($("#id_last_name").val() == "") {
            $('#error_apellido').css('display', 'block');
            error = 1;
        } else {

            $('#error_apellido').css('display', 'none');
        }
        if ($("#id_email").val() == "" || validateEmail($("#id_email").val())) {
            $('#error_correo').css('display', 'block');
            error = 1;
        } else {

            $('#error_correo').css('display', 'none');
        }
        if ($("#id_username").val() == "") {
            $('#error_usernam').css('display', 'block');
            error = 1;
        } else {

            $('#error_usernam').css('display', 'none');
        }
        if ($("#id_password1").val() == "" || $("#id_password2").val() == "") {
            $('#error_contras').css('display', 'block');
            error = 1;
        } else if ($("#id_password1").val() != $("#id_password2").val()) {
            $('#error_recontra').css('display', 'block');
            error = 1;
        } else {

            $('#error_recontra').css('display', 'none');
        }
        if ($("#id_telefono").val() == "") {
            $('#error_telefon').css('display', 'block');
            error = 1;
        } else {

            $('#error_telefon').css('display', 'none');
        }
        if ($("#id_fax").val() == "") {
            $('#error_fax').css('display', 'block');
            error = 1;
        } else {

            $('#error_fax').css('display', 'none');
        }
        if ($("#id_imagen").val() == "") {
            $('#error_imagen').css('display', 'block');
            error = 1;
        } else {

            $('#error_imagen').css('display', 'none');
            error = 0;
        }
        if (error == 0) {
            $('.primero').hide();
            $('.segundo').show();
        }

    });
    $('#continuar2').click(function () {
        var error = 0;
        if ($("#id_pais").val() == "") {
            $('#error_pais').css('display', 'block');
            error = 1;
        } else {

            $('#error_pais').css('display', 'none');
        }
        if ($("#id_estado").val() == "") {
            $('#error_estado').css('display', 'block');
            error = 1;
        } else {

            $('#error_estado').css('display', 'none');
        }
        if ($("#id_ciudad").val() == "") {
            $('#error_ciudad').css('display', 'block');
            error = 1;
        } else {

            $('#error_ciudad').css('display', 'none');
        }
        if ($("#id_codigo_postal").val() == "") {
            $('#error_postal').css('display', 'block');
            error = 1;
        } else {

            $('#error_postal').css('display', 'none');
        }
        if ($("#id_calle").val() == "") {
            $('#error_calle').css('display', 'block');
            error = 1;
        } else {

            $('#error_calle').css('display', 'none');
        }
        if ($("#id_telefono").val() == "") {
            $('#error_telefact').css('display', 'block');
            error = 1;
        } else {

            $('#error_telefact').css('display', 'none');
        }
        if (error == 0) {
            $('.segundo').hide();
            $('.tercero').show();
        }
    });
    $('#continuar3').click(function () {
        var error = 0;
        if ($("#id_paise").val() == "") {
            $('#error_paise').css('display', 'block');
            error = 1;
        } else {

            $('#error_paise').css('display', 'none');
        }
        if ($("#id_estadoe").val() == "") {
            $('#error_estadoe').css('display', 'block');
            error = 1;
        } else {

            $('#error_estadoe').css('display', 'none');
        }
        if ($("#id_ciudade").val() == "") {
            $('#error_ciudade').css('display', 'block');
            error = 1;
        } else {

            $('#error_ciudade').css('display', 'none');
        }
        if ($("#id_codigo_postale").val() == "") {
            $('#error_postale').css('display', 'block');
            error = 1;
        } else {

            $('#error_postale').css('display', 'none');
        }
        if ($("#id_callee").val() == "") {
            $('#error_callee').css('display', 'block');
            error = 1;
        } else {

            $('#error_callee').css('display', 'none');
        }
        if ($("#id_telefonoe").val() == "") {
            $('#error_telenvio').css('display', 'block');
            error = 1;
        } else {

            $('#error_telenvio').css('display', 'none');
        }
        if (error == 0) {
            $('#continuar3').submit();
        }
    })

});
$(function () {
    $('#browse').click(function () {
        $(this).parent().find('input').click();
    });
});
//cargar imagen
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode;
    return ((key >= 48 && key <= 57) || e.keyCode == 9 || e.keyCode == 46)
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
