from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from apps.website.models import Usuario,Testimoniales, Noticias, Suscripciones
from apps.dashboard.models import Configuracion, Servicios, Mensajeria
from .forms import *
from django.core.mail import send_mail
import hashlib, datetime, random
from django.utils import timezone
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.


"""Vistas para control de sesion"""
def index(request):
    noti= Noticias.objects.filter(estado=True).order_by('?')[:3]
    logos=LogosClientes.objects.all()
    testi=Testimoniales.objects.filter(estado=True).prefetch_related("id_usuario").all().order_by('?')[:3]
    if request.user.is_authenticated():
        return render(request, 'website/index.html', {'testimoniales':testi, 'n':range(1, int(6)), 'noticias':noti, 'logos':logos})
    else:
        if request.method == 'POST':
            if iniciar(request):
                if request.GET:
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return render(request, 'website/index.html', {'testimoniales':testi, 'n':range(1, int(6)), 'noticias':noti, 'logos':logos})
    return render(request, 'website/index.html', {'testimoniales':testi, 'n':range(1, int(6)), 'noticias':noti, 'logos':logos})


def iniciar(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            if not user.is_staff:
                login(request, user)
                imagen=Usuario.objects.get(user=request.user.id).imagen
                request.session['imagen'] = "/media/" + str(imagen)
                return True
            else:
                messages.add_message(request, messages.INFO,
                                     _('Cuenta de usuario no válida'))
                return False
        else:
            messages.add_message(request, messages.INFO,
                                 _('Cuenta de usuario inactiva'))
            return False
    else:
        messages.add_message(request, messages.INFO,
                             _('Nombre de usuario o contraseña no válido'))


def iniciar2(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    noti= Noticias.objects.filter(estado=True).order_by('?')[:3]
    logos=LogosClientes.objects.all()
    testi=Testimoniales.objects.filter(estado=True).prefetch_related("id_usuario").all().order_by('?')[:3]
                
    if user is not None:
        if user.is_active:
            if not user.is_staff:
                login(request, user)
                imagen=Usuario.objects.get(user=request.user.id).imagen
                request.session['imagen'] = "/media/" + str(imagen)
                return render(request, 'website/index.html', {'testimoniales':testi, 'n':range(1, int(6)), 'noticias':noti, 'logos':logos})
            else:
                messages.add_message(request, messages.INFO,
                                     _('Cuenta de usuario no válida'))
                return render(request, 'website/index.html')
        else:
            messages.add_message(request, messages.INFO,
                                 _('Cuenta de usuario inactiva'))
            return render(request, 'website/index.html')
    else:
        messages.add_message(request, messages.INFO,
                             _('Nombre de usuario o contraseña no válido'))
    return render(request, 'website/index.html', {'testimoniales':testi, 'n':range(1, int(6)), 'noticias':noti, 'logos':logos})


def salir(request):
    logout(request)
    return redirect('/')


def NoSesion(request):
    return render(request, "website/falta_sesion.html")


"""Vistas Registro de Usuario"""
class RegistrarCliente(CreateView):
    model = User
    form_class = UserForm
    template_name = 'website/registro.html'
    success_url = reverse_lazy('finregistro')
    second_form_class = ClienteForm
    third_form_class = DireccionesFacturacionForm
    fourth_form_class = DireccionesEnvioForm

    def get_context_data(self, **kwargs):
        context = super(RegistrarCliente, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        if 'form3' not in context:
            context['form3'] = self.third_form_class(self.request.GET)
        if 'form4' not in context:
            context['form4'] = self.fourth_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST, request.FILES or None)
        form3 = self.third_form_class(request.POST)
        form4 = self.fourth_form_class(request.POST)
        
        if form.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
            usuario = form.save(commit=False)
            usuario.is_staff = False
            usuario.is_active= False
            usuario.save()
            cliente = form2.save(commit=False)
            cliente.user = usuario
            salt = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()[:10]            
            activation_key = salt           
            key_expires = datetime.datetime.now() + datetime.timedelta(days = 2)
            print(key_expires)
            cliente.activation_key=activation_key
            cliente.key_expires=key_expires
            cliente.save()
            direccion1 = form3.save(commit=False)
            direccion1.id_usuario = usuario
            direccion1.tipo_direccion="Facturacion"
            direccion1.save()
            direccion2 = form4.save(commit=False)
            direccion2.id_usuario = usuario
            direccion2.tipo_direccion="Envio"
            direccion2.save()
            #enviar email con tocken de activacion
            email_subject = 'Enlace de Activacion'
            email_body = "Hola %s, Gracias por registrarte. Para activar tu cuenta da clíck en este link en menos de 48 horas: http://localhost:8000/activar/%s" % (usuario.username, activation_key)

            send_mail(email_subject, email_body, usuario.email,
                [usuario.email], fail_silently=False)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(
                    form=form, form2=form2,form3=form3, form4=form4))


def finRegistro(request):
    return render(request, "website/fin_registro.html")


def ConfirmarRegistro(request, activation_key):
    # Verifica que el usuario ya está logeado
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # Verifica que el token de activación sea válido y sino retorna un 404
    user_profile = get_object_or_404(Usuario, activation_key=activation_key)

    # verifica si el token de activación ha expirado y si es así renderiza el html de registro expirado
    if user_profile.key_expires < timezone.now():
        return render(request,'website/token_vencido.html')
    # Si el token no ha expirado, se activa el usuario y se muestra el html de confirmación
    user = user_profile.user
    user.is_active = True
    user.save()
    return render(request,'website/cuenta_activa.html')


"""Vistas para Perfil de Usuario"""
@csrf_exempt
def perfil(request,id):
    usuario=User.objects.get(id=id)
    direccion1=DireccionFacturacionUsuario.objects.filter(id_usuario=id)
    direccion2=DireccionEnvioUsuario.objects.filter(id_usuario=id)
    instance = Usuario.objects.get(user=id)
    form = EditarFotoForm(request.POST,request.FILES, instance=instance)

    if request.method == 'POST':
        if form.is_valid():
            form.save()

    return render(request, "website/perfil.html", {'usuario':usuario, 'facturacion':direccion1, 'envio':direccion2, 'foto':form})


@csrf_exempt
def perfil_modificar(request, id):
    if request.is_ajax():
        usuario = User.objects.get(id=id)
        usuario.first_name=request.POST['nombre']
        usuario.last_name=request.POST['apellido']
        usuario.email=request.POST['correo']
        cliente=Usuario.objects.get(user=id)
        cliente.telefono=request.POST['telefono']
        cliente.fax=request.POST['fax']
        usuario.save()
        cliente.save()
        return HttpResponse(json.dumps({'mensaje': 'Perfil de usuario actualizado satisfactoriamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def direccionFac_modificar(request, id):
    if request.is_ajax():
        dire = DireccionFacturacionUsuario.objects.get(id_direccion=id)
        dire.pais=request.POST['pais']
        dire.estado=request.POST['estado']
        dire.ciudad=request.POST['ciudad']
        dire.calle=request.POST['calle']
        dire.codigo_postal=request.POST['codigo_postal']
        dire.telefono=request.POST['telefono']
        dire.telefono2=request.POST['telefono2']
        dire.save()
        return HttpResponse(json.dumps({'mensaje': 'Direccion actulizada satisfactoriamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def direccionEnv_modificar(request, id):
    if request.is_ajax():
        dire = DireccionEnvioUsuario.objects.get(id_direccion=id)
        dire.paise=request.POST['pais']
        dire.estadoe=request.POST['estado']
        dire.ciudade=request.POST['ciudad']
        dire.callee=request.POST['calle']
        dire.codigo_postale=request.POST['codigo_postal']
        dire.telefonoe=request.POST['telefono']
        dire.telefonoe2=request.POST['telefono2']
        dire.save()
        return HttpResponse(json.dumps({'mensaje': 'Direccion actulizada satisfactoriamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def clave_modificar(request, id):
    if request.is_ajax():
        usuario = User.objects.get(id=id)
        usuario.set_password(request.POST['clavenueva'])
        usuario.save()
        return HttpResponse(json.dumps({'mensaje': 'Contraseña modificada satisfactoriamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def direccion_crear(request):
    if request.is_ajax():
        usuario= User.objects.get(id=int(request.POST['usuario']))
        if int(request.POST['tipo']) == 1:
            dire = DireccionFacturacionUsuario(id_usuario=usuario,pais=request.POST['pais'],estado=request.POST['estado'],ciudad=request.POST['ciudad'],calle=request.POST['calle'],codigo_postal=request.POST['codigo_postal'],telefono=request.POST['telefono'],telefono2=request.POST['telefono2'])
            dire.save()
        if int(request.POST['tipo']) == 2:
            dire = DireccionEnvioUsuario(id_usuario=usuario,paise=request.POST['pais'],estadoe=request.POST['estado'],ciudade=request.POST['ciudad'],callee=request.POST['calle'],codigo_postale=request.POST['codigo_postal'],telefonoe=request.POST['telefono'],telefonoe2=request.POST['telefono2'])
            dire.save()
        return HttpResponse(json.dumps({'mensaje': 'Direccion creada satisfactoriamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


"""Vista para Formulario de Contacto"""
@csrf_exempt
def enviar_contacto(request):
    if request.is_ajax():
        mensaje = Mensajeria(nombre=request.POST['nombre'],apellido=request.POST['apellido'],correo=request.POST['correo'],mensaje=request.POST['mensaje'])
        mensaje.save()
        #enviar email
        email_subject = 'Mensaje recibido'
        email_body = "Hola %s, Gracias por contactar con nosotros. Hemos recibido tu mensaje, y responderemos a la brevedad posible." % (request.POST['nombre'])
        send_mail(email_subject, email_body, request.POST['correo'],
                [request.POST['correo']], fail_silently=False)
        return HttpResponse(json.dumps({'mensaje': 'Hemos recibido su mensaje, responderemos lo antes posible. Gracias por contactarnos.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


"""Vista para Formulario de suscripcion"""
@csrf_exempt
def suscripcion(request):
    if request.is_ajax():
        s = Suscripciones(nombre=request.POST['nombre'],correo=request.POST['correo'])
        s.save()
        return HttpResponse(json.dumps({'mensaje': 'Desde ahora esta suscrito para recibir nuestros boletines informativos. Gracias.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


"""Vistas para index"""
def noticias(request, id):
    noti = Noticias.objects.get(id_noticia=id)
    interes= Noticias.objects.filter(estado=True).exclude(id_noticia=id).order_by('?')[:3]
    return render(request, "website/noticias.html", {'noticia':noti, 'interes':interes})


def nosotros(request):
    c=Configuracion.objects.all()
    if c:
        conf=c[0]
        imagen="/media/" + str(conf.imagen1nosotros)
        imagen2="/media/" + str(conf.imagen2nosotros)
        texto=conf.descripcion_nosotros
    else:
        imagen=""
        texto=""
    return render(request, "website/nosotros.html", {'imagen': imagen,'imagen2': imagen2, 'texto':texto})


def informacion(request):
    c=Configuracion.objects.all()
    if c:
        conf=c[0]
        imagen="/media/" + str(conf.imagen1info)
        imagen2="/media/" + str(conf.imagen2info)
        imagen3="/media/" + str(conf.imagen3info)
        lista=conf.descripcion_info.split()
        texto = [' '.join(lista[i:i + 30]) for i in range(0, len(lista), 30)]
    else:
        imagen=""
        texto=""
    return render(request, "website/informacion.html", {'imagen': imagen,'imagen2': imagen2,'imagen3': imagen3, 'texto':texto})


def listado(request):
    return render(request, "website/listado.html")


"""Vistas para Servicios"""
def servicios(request):
    s1=Servicios.objects.filter(tipo=1)[0]
    s2=Servicios.objects.filter(tipo=2)[0]
    s3=Servicios.objects.filter(tipo=3)[0]
    s4=Servicios.objects.filter(tipo=4)[0]

    return render(request, "website/servicioss.html", {'aereo':s1,'mar':s2,'almacen':s3,'puerta':s4})








def detalle(request):
    return render(request, "website/detalle_producto.html")


def proceso_compra(request):
    return render(request, "website/proceso_compra.html")


def caja(request):
    return render(request, "website/caja.html")


def servicioss(request):
    return render(request, "website/servicioss.html")
