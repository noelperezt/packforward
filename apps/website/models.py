from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from apps.dashboard.models import Productos
from django.utils import timezone
import datetime
# Create your models here.


class Noticias(models.Model):
    id_noticia = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=250, null=False,
        blank=False, default="", verbose_name=_('titulo de la noticia'))
    imagen = models.ImageField(
        upload_to='noticias/', null=True, blank=True, default="", verbose_name=_('imagen de la noticia'))
    info_imagen = models.CharField(default="", max_length=150, null=False,
        blank=False, verbose_name=_('reseña de la imagen'))
    contenido_sec1= models.TextField(
        null=False, blank=False, default="", verbose_name=_('cuerpo de la noticia´parte 1'))
    contenido_resaltado= models.TextField(
        null=False, blank=False, default="", verbose_name=_('texto resaltado de la noticia'))
    contenido_sec2= models.TextField(
        null=False, blank=False, default="", verbose_name=_('cuerpo de la noticia´parte 2'))
    fecha = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de la noticia'))
    estado = models.BooleanField(
        max_length=20, null=False, blank=False, default=True, verbose_name=_('estado de la noticia'))

    class Meta:
        db_table = 'noticia'
        verbose_name = 'noticia'


class ZonasBanners(models.Model):
    id_zona = models.AutoField(primary_key=True)
    ubicacion = models.CharField(
        max_length=250, null=False, blank=False, verbose_name=_('ubicacion de la zona'))
    estado = models.CharField(
        max_length=20, null=False, blank=False, verbose_name=_('estado de la zona'))

    class Meta:
        db_table = 'zona_banner'
        verbose_name = 'zonas de banners'


class Banners(models.Model):
    id_banner = models.AutoField(primary_key=True)
    id_zona = models.ForeignKey(
        ZonasBanners, null=True, blank=True, verbose_name=_('zona relacionada'))
    url = models.TextField(null=False, blank=False,
                           verbose_name=_('url enlace del banner'))
    imagen = models.ImageField(
        upload_to='banners/', null=False, blank=False, verbose_name=_('imagen del banner'))
    fecha_inicio = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de inicio'))
    fecha_fin = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de finalizacion'))
    estado = models.BooleanField(
        blank=False, default=True, verbose_name=_('estado del banner'))

    class Meta:
        db_table = 'banner'
        verbose_name = 'banners'


class Testimoniales(models.Model):
    id_testimonial = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    testimonio = models.TextField(
        null=False, blank=False, verbose_name=_('testimonio'))
    puntuacion = models.IntegerField(
        default=0, null=False, blank=False, verbose_name=_('puntuacion asignada'))
    fecha = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha del testimonio'))
    estado = models.BooleanField(
        blank=False, default=True, verbose_name=_('estado del banner'))

    class Meta:
    	db_table = 'testimonial'
    	verbose_name = 'testimonial'


class Usuario(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telefono = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('telefono'))
    fax = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('fax'))
    imagen = models.ImageField(upload_to='profile_photo/', null=False,
                               blank=True, verbose_name=_('imagen de usuario'))
    empresa = models.BooleanField(
        blank=False, default=False, verbose_name=_('cliente empresa'))
    activation_key = models.CharField(default="", max_length=40, blank=True)
    key_expires = models.DateTimeField(default=timezone.now)

    class Meta:
    	db_table = 'usuario'
    	verbose_name = 'usuario'


class CarritoCompra(models.Model):
    id_carrito = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    cantidad_articulos = models.IntegerField(default=0, null=False, blank=False, verbose_name=_(
        'cantidad de piezas en el articulos en el carrito'))

    class Meta:
        db_table = 'carrito_compra'
        verbose_name = 'carrito de compra'


class DetalleCarrito(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_carrito = models.ForeignKey(
        CarritoCompra, null=True, blank=True, verbose_name=_('carrito de compra relacionado'))
    id_producto = models.OneToOneField(Productos, null=True, blank=True, default=0, verbose_name=_('producto relacionado'))
    cantidad = models.IntegerField(
        default=0, null=False, blank=False, verbose_name=_('cantidad del producto'))
    precio = models.DecimalField(max_digits=19, decimal_places=3, default=0.0,
                                 null=False, blank=False, verbose_name=_('precio del producto'))
    fecha = models.DateField(auto_now_add=True, editable=True, verbose_name=_(
        'fecha de agregado al carrito'))

    class Meta:
        db_table = 'detalle_carrito'
        verbose_name = 'detalle del carrito de compra'


class Compra(models.Model):
    id_compra = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    fecha = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de la compra'))
    observaciones = models.CharField(
        max_length=500, null=False, blank=False, verbose_name=_('observaciones de la compra'))
    estado = models.CharField(
        max_length=20, null=False, blank=False, verbose_name=_('Estado de la Compra'))

    class Meta:
        db_table = 'compra'
        verbose_name = 'compra'


class DetalleCompra(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_compra = models.ForeignKey(
        Compra, null=True, blank=True, verbose_name=_('ccompra relacionada'))
    id_producto = models.IntegerField(
        default=0, null=True, blank=True, verbose_name=_('producto relacionado'))
    cantidad = models.IntegerField(
        default=0, null=False, blank=False, verbose_name=_('cantidad del producto'))
    precio = models.DecimalField(max_digits=19, decimal_places=3, default=0.0,
                                 null=False, blank=False, verbose_name=_('precio del producto'))

    class Meta:
        db_table = 'detalle_compra'
        verbose_name = 'detalle de la compra'


class Alertas(models.Model):
    id_alerta = models.AutoField(primary_key=True)
    id_compra = models.ForeignKey(
        Compra, null=True, blank=True, verbose_name=_('compra relacionada'))
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    titulo = models.CharField(
        max_length=50, null=False, blank=False, verbose_name=_('titulo de la alerta'))
    descripcion = models.TextField(
        max_length=500, null=False, blank=False, verbose_name=_('contenido de la alerta'))
    fecha = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de la alerta'))
    estado = models.BooleanField(
        blank=False, default=True, verbose_name=_('estado de la alerta'))

    class Meta:
        db_table = 'alerta'
        verbose_name = 'alerta'


class Suscripciones(models.Model):
    id_suscripcion = models.AutoField(primary_key=True)
    nombre = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_('nombre del suscriptor'))
    correo = models.TextField(
        max_length=250, null=False, blank=False, verbose_name=_('correo del suscriptor'))

    class Meta:
        db_table = 'suscripcion'
        verbose_name = 'suscripcion'