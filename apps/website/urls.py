from django.conf.urls import url
from .views import *
from django.contrib.auth.decorators import login_required

urlpatterns = [
    #Secciones de Pagina de inicio
    url(r'^$', index, name='index'),
    url(r'^noticias/(?P<id>\d+)/$', noticias, name='noticias'),
    url(r'^servicios/$', servicios, name='servicios'),
    url(r'^nosotros/$', nosotros, name='nosotros'),
    url(r'^informacion/$', informacion, name='informacion'),
    #Control de Sesion
    url(r'^login/$', iniciar2, name='login'),
    url(r'^salir/$', salir, name='cerrar_sesion'),
    url(r'^registro/$', RegistrarCliente.as_view(), name='registro'),
    url(r'^finregistro/$', finRegistro, name='finregistro'),
    url(r'^activar/(?P<activation_key>\w+)/', ConfirmarRegistro),
    url(r'^sinsesion/$', NoSesion, name='nosesion'),
    #Perfil de Usuario
    url(r'^perfil/(?P<id>\d+)/$', login_required(perfil,login_url='/sinsesion'), name='perfil'),
    url(r'^editarperfil/(?P<id>\d+)$', login_required(perfil_modificar,login_url='/sinsesion'), name='editar_perfil'),
    url(r'^editardirfac/(?P<id>\d+)$', login_required(direccionFac_modificar,login_url='/sinsesion'), name='editar_dirfac'),
    url(r'^editardirenv/(?P<id>\d+)$', login_required(direccionEnv_modificar,login_url='/sinsesion'), name='editar_direnv'),
    url(r'^nuevadir/$', login_required(direccion_crear,login_url='/sinsesion'), name='nueva_dir'),
    url(r'^editarclave/(?P<id>\d+)$', login_required(clave_modificar,login_url='/sinsesion'), name='editar_clave'),
    #Formulario de Contacto
    url(r'^contacto/$', enviar_contacto, name='enviar_contacto'),
    #Formulario de suscripcion
    url(r'^suscripcion/$', suscripcion, name='suscripcion'),
    
    url(r'^detalle/$', detalle, name='detalle'),
    url(r'^listado/$', listado, name='listado'),
    url(r'^compra/$', proceso_compra, name='compra'),
    url(r'^caja/$', caja, name='caja'),
    url(r'^servicios/$', caja, name='servicioss'),
]
