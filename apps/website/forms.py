from django import forms
from django.contrib.auth.models import User
from apps.dashboard.models import DireccionFacturacionUsuario,DireccionEnvioUsuario, LogosClientes
from apps.website.models import *
from django.core.exceptions import ValidationError
from django.forms import EmailInput, TextInput
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import FileInput

'''Formularios para CLientes'''
class UserForm(forms.ModelForm):
    password1 = forms.CharField(
        label=_('Contraseña'),
        widget=forms.PasswordInput(attrs={'class': 'form-control  border-input','placeholder':'Contraseña'}))
    password2 = forms.CharField(
        label=_('Repetir Contraseña'),
        widget=forms.PasswordInput(attrs={'class': 'form-control  border-input','placeholder':'Confirmar Contraseña'}))

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username")
        widgets = {
            'first_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Nombre'}),
            'last_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Apellido'}),
            'email':
            EmailInput(attrs={'class': 'form-control  border-input',
                              'required': 'required','placeholder':'Correo Electronico'}),
            'username':
            TextInput(attrs={'class': 'form-control  border-input','required': 'required','placeholder':'Usuario'}),
        }

    def clean_password2(self):
        """Validación de contraseñas iguales."""
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            msg = _('Las contraseñas no coinciden')
            raise forms.ValidationError(msg)
        return password2

    def save(self, commit=True):
        """Redefinición de método ``save()`` para guardar la contraseña."""
        try:
            user = super(UserForm, self).save(commit=False)
            user.set_password(self.cleaned_data["password1"])
        except ValidationError:
            msg = _('No se pudo guardar el Usuario')
            raise forms.ValidationError(msg)
        if commit:
            user.save()
        return user


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ("telefono","fax", "imagen", "empresa")
        widgets = {
            'telefono':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Telefono', 'onKeyPress':'return soloNumeros(event);'}),
            'fax':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Fax', 'onKeyPress':'return soloNumeros(event);'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none','required': 'required'}),
            'empresa':
            forms.CheckboxInput(attrs={'class': 'form-control checkbox-inline'})
        }


class EditarFotoForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('imagen',)
        widgets = {
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none','required': 'required'})
        }


class DireccionesFacturacionForm(forms.ModelForm):
    class Meta:
        model = DireccionFacturacionUsuario
        fields = ("pais", "estado", "ciudad", "calle", "codigo_postal", "telefono", "telefono2")
        widgets = {
            'pais':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Pais'}),
            'estado':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Estado'}),
            'ciudad':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Ciudad'}),
            'calle':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Calle o Avenida'}),
            'codigo_postal':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Codigo Postal'}),
            'telefono':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Telefono', 'onKeyPress':'return soloNumeros(event);'}),
            'telefono2':
            TextInput(attrs={'class': 'form-control  border-input','placeholder':'Telefono Secundario', 'onKeyPress':'return soloNumeros(event);'}),
        }


class DireccionesEnvioForm(forms.ModelForm):
    class Meta:
        model = DireccionEnvioUsuario
        fields = ("paise", "estadoe", "ciudade", "callee", "codigo_postale", "telefonoe", "telefonoe2")
        widgets = {
            'paise':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Pais'}),
            'estadoe':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Estado'}),
            'ciudade':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Ciudad'}),
            'callee':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Calle o Avenida'}),
            'codigo_postale':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Codigo Postal'}),
            'telefonoe':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Telefono', 'onKeyPress':'return soloNumeros(event);'}),
            'telefonoe2':
            TextInput(attrs={'class': 'form-control  border-input','placeholder':'Telefono Secundario', 'onKeyPress':'return soloNumeros(event);'}),
        }
