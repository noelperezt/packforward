from django.conf.urls import url
from .views import * 
from django.contrib.auth.decorators import login_required

urlpatterns = [
    # URLs de Sesion
    url(r'^$', base, name='dashboard'),
    url(r'^login/$', iniciar, name='login'),
    url(r'^salir/$', salir, name='cerrar_sesion'),
    # URLs de Usuarios Administradores
    url(r'^search/$', BuscarUsuario, name='search'),
    url(r'^permisos/$', login_required(CargarPermisos,login_url='/dashboard/'), name='permisos'),
    url(r'^listuser', login_required(ListarUser.as_view(),login_url='/dashboard/'), name='listar_user'),
    url(r'^crearuser/$', login_required(CrearUser.as_view(),login_url='/dashboard/'), name='crear_user'),
    url(r'^editaruser/(?P<pk>\d+)$', login_required(ModificarUser.as_view(),login_url='/dashboard/'), name='editar_user'),
    url(r'^eliminaruser/(?P<pk>\d+)/$',login_required(EliminarUser.as_view(),login_url='/dashboard/'), name='eliminar_user'),
    # URLs de Usuarios Clientes
    url(r'^searchc/$', BuscarCliente, name='searchc'),
    url(r'^listclientes', login_required(ListarClientes.as_view(),login_url='/dashboard/'), name='listar_clientes'),
    url(r'^crearcliente/$', login_required(CrearCliente.as_view(),login_url='/dashboard/'), name='crear_cliente'),
    url(r'^editarcliente/(?P<pk>\d+)$', login_required(ModificarCliente.as_view(),login_url='/dashboard/'), name='editar_cliente'),
    url(r'^estadocliente/(?P<id>\d+)/$', login_required(EstadoCliente,login_url='/dashboard/'), name='estado_cliente'),
    # URLs de Configuracion Basica
    url(r'^configuracion/$', login_required(CrearConfiguracion.as_view(),login_url='/dashboard/'), name='crear_configuracion'),
    url(r'^configuracion/(?P<pk>\d+)/$', login_required(EditarConfiguracion.as_view(),login_url='/dashboard/'), name='configuracion'),
    # URLs de Marcas
    url(r'^listmarca', login_required(ListarMarcas.as_view(),login_url='/dashboard/'), name='listar_marcas'),
    url(r'^crearmarca/$', login_required(CrearMarcas.as_view(),login_url='/dashboard/'), name='crear_marca'),
    url(r'^editarmarca/(?P<pk>\d+)$', login_required(ModificarMarcas.as_view(),login_url='/dashboard/'), name='editar_marca'),
    url(r'^eliminarmarca/(?P<pk>\d+)/$',login_required(EliminarMarcas.as_view(),login_url='/dashboard/'), name='eliminar_marca'),
    url(r'^cargarmarcas$', login_required(cargar_marcas,login_url='/dashboard/'),name='cargar_marcas'),
    # URLs de Modelos
    url(r'^listmodelo', login_required(ListarModelos.as_view(),login_url='/dashboard/'), name='listar_modelos'),
    url(r'^crearmodelo/$', login_required(CrearModelos.as_view(),login_url='/dashboard/'), name='crear_modelo'),
    url(r'^editarmodelo/(?P<pk>\d+)$', login_required(ModificarModelos.as_view(),login_url='/dashboard/'), name='editar_modelo'),
    url(r'^eliminarmodelo/(?P<pk>\d+)/$',login_required(EliminarModelos.as_view(),login_url='/dashboard/'), name='eliminar_modelo'),
    # URLs de Testimoniales
    url(r'^listartesti', login_required(ListarTestimoniales.as_view(),login_url='/dashboard/'), name='listar_test'),
    url(r'^creartest/$', login_required(CrearTestimonial.as_view(),login_url='/dashboard/'), name='crear_test'),
    url(r'^editartest/(?P<pk>\d+)/$', login_required(ModificarTestimonial.as_view(),login_url='/dashboard/'), name='editar_test'),
    url(r'^estadotest/(?P<id>\d+)/$', login_required(EstadoTestimonial,login_url='/dashboard/'), name='estado_test'),
    url(r'^eliminartest/(?P<pk>\d+)/$',login_required(EliminarTestimonial.as_view(),login_url='/dashboard/'), name='eliminar_test'),
    # URLs de Servicios
    url(r'^listarserv', login_required(ListarServicios.as_view(),login_url='/dashboard/'), name='listar_serv'),
    url(r'^crearserv/$', login_required(CrearServicios.as_view(),login_url='/dashboard/'), name='crear_serv'),
    url(r'^editarserv/(?P<pk>\d+)/$', login_required(EditarServicios.as_view(),login_url='/dashboard/'), name='editar_serv'),
    url(r'^eliminarserv/(?P<pk>\d+)/$',login_required(EliminarServicios.as_view(),login_url='/dashboard/'), name='eliminar_serv'),
    # URLs de Noticias
    url(r'^listarnoti', login_required(ListarNoticias.as_view(),login_url='/dashboard/'), name='listar_noti'),
    url(r'^crearnoti/$', login_required(CrearNoticias.as_view(),login_url='/dashboard/'), name='crear_noti'),
    url(r'^editarnoti/(?P<pk>\d+)/$', login_required(EditarNoticias.as_view(),login_url='/dashboard/'), name='editar_noti'),
    url(r'^estadonoti/(?P<id>\d+)/$', login_required(EstadoNoticias,login_url='/dashboard/'), name='estado_noti'),
    url(r'^eliminarnoti/(?P<pk>\d+)/$',login_required(EliminarNoticias.as_view(),login_url='/dashboard/'), name='eliminar_noti'),
    # URLs de Noticias
    url(r'^listarmsj/', login_required(ListarMensajes.as_view(),login_url='/dashboard/'), name='listar_mensajes'),
    url(r'^vermsj/(?P<id>\d+)/$', login_required(VerMensaje,login_url='/dashboard/'), name='ver_mensaje'),
    url(r'^responder/$', login_required(enviar_respuesta,login_url='/dashboard/'), name='enviar_respuesta'),
    # URLs de Noticias
    url(r'^logos/', login_required(Logos,login_url='/dashboard/'), name='logos'),
    url(r'^logoelim/(?P<id>\d+)/$', login_required(LogosEliminar,login_url='/dashboard/'), name='eliminar_logo'),


    
]
