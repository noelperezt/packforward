"""
 Formularios de la aplicación Administrador.
"""
from django import forms
from django.contrib.auth.models import User
from apps.dashboard.models import *
from apps.website.models import Usuario, Testimoniales, Noticias
from django.core.exceptions import ValidationError
from django.forms import EmailInput, TextInput
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import FileInput


'''Formularios para Usuarios Administradores'''
class UserForm(forms.ModelForm):
    password1 = forms.CharField(
        label=_('Contraseña'),
        widget=forms.PasswordInput(attrs={'class': 'form-control  border-input','placeholder':'Contraseña'}))
    password2 = forms.CharField(
        label=_('Repetir Contraseña'),
        widget=forms.PasswordInput(attrs={'class': 'form-control  border-input','placeholder':'Confirmar Contraseña'}))

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username")
        widgets = {
            'first_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Nombre'}),
            'last_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Apellido'}),
            'email':
            EmailInput(attrs={'class': 'form-control  border-input',
                              'required': 'required','placeholder':'Correo Electronico'}),
            'username':
            TextInput(attrs={'class': 'form-control  border-input','required': 'required','placeholder':'Usuario'}),
        }

    def clean_password2(self):
        """Validación de contraseñas iguales."""
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            msg = _('Las contraseñas no coinciden')
            raise forms.ValidationError(msg)
        return password2

    def save(self, commit=True):
        """Redefinición de método ``save()`` para guardar la contraseña."""
        try:
            user = super(UserForm, self).save(commit=False)
            user.set_password(self.cleaned_data["password1"])
        except ValidationError:
            msg = _('No se pudo guardar el Usuario')
            raise forms.ValidationError(msg)
        if commit:
            user.save()
        return user


class UserFormEdit(forms.ModelForm):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username")
        widgets = {
            'first_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Nombre'}),
            'last_name':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Apellido'}),
            'email':
            EmailInput(attrs={'class': 'form-control  border-input',
                              'required': 'required','placeholder':'Correo Electronico'}),
            'username':
            TextInput(attrs={'class': 'form-control  border-input','required': 'required','placeholder':'Usuario'}),
        }


class AdministradorForm(forms.ModelForm):
    class Meta:
        model = Administradores
        fields = ("pais", "ciudad", "imagen")
        widgets = {
            'pais':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Pais'}),
            'ciudad':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Ciudad'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none','required': 'required'}),
        }


class AdministradorFormEdit(forms.ModelForm):
    class Meta:
        model = Administradores
        fields = ("pais", "ciudad", "imagen")
        widgets = {
            'pais':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Pais'}),
            'ciudad':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Ciudad'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none'}),
        }


'''Formularios para Usuarios Clientes'''
class ClienteForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ("telefono","fax", "imagen","empresa")
        widgets = {
            'telefono':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Telefono', 'onKeyPress':'return soloNumeros(event);'}),
            'fax':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Fax', 'onKeyPress':'return soloNumeros(event);'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none','required': 'required'}),
            'empresa':
            forms.CheckboxInput(attrs={'class': 'form-control checkbox-inline'})
        }


class ClienteFormEdit(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ("telefono","fax","imagen","empresa")
        widgets = {
            'telefono':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Telefono', 'onKeyPress':'return soloNumeros(event);'}),
            'fax':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Fax', 'onKeyPress':'return soloNumeros(event);'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none'}),
            'empresa':
            forms.CheckboxInput(attrs={'class': 'form-control checkbox-inline'})
        }


'''Formularios para Configuracion de valores iniciales'''
class ConfiguracionForm(forms.ModelForm):
    class Meta:
        model = Configuracion
        fields = ("imagen1nosotros","imagen2nosotros", "descripcion_nosotros", "imagen1info","imagen2info","imagen3info","descripcion_info","precio_usd", "pago_bs")
        widgets = {
            'imagen1nosotros':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none'}),
            'imagen2nosotros':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL2(this);',
                              'style':'display: none'}),
            'imagen1info':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL3(this);',
                              'style':'display: none'}),
            'imagen2info':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL4(this);',
                              'style':'display: none'}),
            'imagen3info':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL5(this);',
                              'style':'display: none'}),
            'descripcion_nosotros':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Descripción de la empresa'}),
            'descripcion_info':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Descripción de Informacion'}),
            'precio_usd':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Precio de USD a Bs.'}),
            'pago_bs':
            forms.CheckboxInput(attrs={'class': 'form-control checkbox-inline'})
        }


'''Formulario para Marcas'''
class MarcaForm(forms.ModelForm):
    class Meta:
        model = Marca
        fields = ("nombre",)
        widgets = {
            'nombre':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Nombre de la Marca'}),
        }


'''Formulario para Modelos'''
class ModeloForm(forms.ModelForm):
    class Meta:
        model = Modelo
        fields = ("nombre","id_marca")
        widgets = {
            'id_marca': forms.Select(attrs={'class': 'form-control border-input',
                             'required': 'required'}),
            'nombre':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Nombre del modelo'}),
        }


'''Formulario para Testimoniales'''
class TestimonialesForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['id_usuario'].widget.attrs.update({'class': 'form-control border-input','required': 'required', 'title': 'Cliente'})
    id_usuario = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=False))
    class Meta:
        model = Testimoniales    
        fields = ("id_usuario","testimonio", "puntuacion")
        widgets = {
            'testimonio':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Testimonio del Usuario'}),
            'puntuacion':forms.HiddenInput(),
        }


'''Formulario para Servicios'''
class ServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicios    
        fields = ("titulo","encabezado", "imagen_detalle", "descripcion",
            "descuento", "maneja_seguro", "divisor_pg", "divisor_cm",
            "precio_lb", "precio_kg", "tipo")
        widgets = {
            'titulo':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Titulo'}),
            'encabezado':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none'}),
            'imagen_detalle':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL2(this);',
                              'style':'display: none'}),
            'descripcion':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'descripcion', 'style':'height: 291px;'}),
            'descuento':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Porcentaje de descuento'}),
            'maneja_seguro':
            forms.CheckboxInput(attrs={'class': 'form-control checkbox-inline'}),
            'divisor_pg':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Divisor por Pulgada'}),
            'divisor_cm':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Divisor por Centimetro'}),
            'precio_lb':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Precio por Libra'}),
            'precio_kg':
            forms.NumberInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Precio por Kilogramo'}),
            'tipo':forms.HiddenInput(),
        }


'''Formulario para Noticias'''
class NoticiasForm(forms.ModelForm):
    class Meta:
        model = Noticias   
        fields = ("titulo","imagen", "info_imagen", "contenido_sec1",
            "contenido_resaltado", "contenido_sec2")
          
        widgets = {
            'titulo':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Titulo'}),
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'style':'display: none'}),
            'info_imagen':
            TextInput(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Info para la Imagen'}),
            'contenido_sec1':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Contenido Seccion 1', 'style':'height: 200px;'}),
            'contenido_sec2':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Contenido Seccion 2', 'style':'height: 200px;'}),
            'contenido_resaltado':
            forms.Textarea(attrs={'class': 'form-control  border-input',
                             'required': 'required','placeholder':'Contenido Resaltado', 'style':'height: 150px;'}),
        }


'''Formulario pa Logos de Clientes'''
class LogosClientesForm(forms.ModelForm):
    class Meta:
        model = LogosClientes
        fields = ('imagen',)
        widgets = {
            'imagen':
            FileInput(attrs={'class': 'form-control', 'onchange':'readURL(this);',
                              'required': 'required','style':'display: none'})
        }