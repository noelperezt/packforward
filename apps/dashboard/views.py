from django.shortcuts import render, redirect, HttpResponseRedirect
from django.http import HttpResponseBadRequest, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.contrib.auth.models import User
from apps.dashboard.models import *
from apps.website.models import Usuario
from .forms import *
from django.core.urlresolvers import reverse_lazy
import uuid
import json
from django.db.models import Q
from django.core import serializers
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import hashlib, datetime, random
from django.utils import timezone
from django.core.mail import send_mail

# Create your views here.

"""Vistas para control de sesion"""
def base(request):
    if request.user.is_authenticated():
        return render(request, 'dashboard/base.html')
    else:
        if request.method == 'POST':
            if iniciar(request):
                if request.GET:
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return render(request, 'dashboard/base.html')
            else:
                return render(request, 'dashboard/usuario_invalido.html')
    return render(request, 'dashboard/ini_sesion.html')


def iniciar(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            if user.is_staff:
                login(request, user)
                conf=0
                cant = Configuracion.objects.count()
                if cant>0:
                    conf=Configuracion.objects.all()[0].id_configuracion
                request.session['conf'] = conf
                if user.is_superuser:
                    lista=[1,2,3,4,5]
                    imagen="/static/images/user_log.png"
                else:
                    per=Permisos.objects.filter(user=request.user.id)
                    lista=[]
                    for p in per:
                        lista.append(p.permiso)
                    imagen="/media/" + str(Administradores.objects.get(user=request.user.id).imagen)
                
                request.session['permisos'] = lista
                request.session['imagen'] = imagen
                return True
            else:
                messages.add_message(request, messages.INFO,
                                     _('Cuenta de usuario no válida'))
                return False
        else:
            messages.add_message(request, messages.INFO,
                                 _('Cuenta de usuario inactiva'))
            return False
    else:
        messages.add_message(request, messages.INFO,
                             _('Nombre de usuario o contraseña no válido'))


def salir(request):
    logout(request)
    return redirect('/dashboard/')


"""Vistas CRUD Usuarios Administradores"""
class ListarUser(ListView):
    model = User
    template_name = 'dashboard/listar_user.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = User.objects.filter(is_staff=True, is_superuser=False)
        return queryset


@csrf_exempt
def BuscarUsuario(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    buscado = request.POST['buscado']
    palabras = buscado.split('%20')
    concat = None
    for palabra in palabras:
        if concat is None:
            concat = Q(first_name__icontains=palabra)
        else:
            concat = concat & Q(first_name__icontains=palabra)

    lista = User.objects.filter(concat).exclude(is_superuser=True).exclude(is_staff=False)
    dic = {}
    for i in lista:
        dic.update(
                        {i.id:{
                            'first_name':i.first_name,
                            'last_name':i.last_name,
                            'email':i.email,
                            'username':i.username,
                            'is_active':i.is_active,
                            'imagen':str(Administradores.objects.get(user=i.id).imagen),
                            }
                        })
    object_list=json.dumps(dic)
    return HttpResponse(object_list, content_type="application/json")


class CrearUser(CreateView):
    model = User
    form_class = UserForm
    template_name = 'dashboard/crear_user.html'
    success_url = reverse_lazy('listar_user')
    second_form_class = AdministradorForm

    def get_context_data(self, **kwargs):
        context = super(CrearUser, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST, request.FILES or None)
        permisos = request.POST.getlist('permisos')
        if form.is_valid() and form2.is_valid() and permisos:
            usuario = form.save(commit=False)
            usuario.is_staff = True
            usuario.save()
            administrador = form2.save(commit=False)
            administrador.user = usuario
            administrador.save()
            for pe in permisos:
                p = Permisos(user=usuario, permiso=int(pe))
                p.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            if not permisos:
                messages.add_message(request, messages.INFO,
                                     _('Seleccione los permisos.'))
            return self.render_to_response(
                self.get_context_data(
                    form=form, form2=form2))


class ModificarUser(UpdateView):
    model = User
    second_model = Administradores
    form_class = UserFormEdit
    second_form_class = AdministradorFormEdit
    template_name = 'dashboard/modif_user.html'
    success_url = reverse_lazy('listar_user')

    def get_context_data(self, **kwargs):
        context = super(ModificarUser, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        usuario = self.model.objects.get(id=pk)
        administrador = self.second_model.objects.get(
            user=usuario.id)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=administrador)
            context['id'] = pk
            img = Administradores.objects.get(user=pk)
            context['imagen'] = "/media/" + str(img.imagen)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id = kwargs['pk']
        usuario = self.model.objects.get(id=id)
        administrador = self.second_model.objects.get(
            user=usuario.id)
        form = self.form_class(request.POST, instance=usuario)
        form2 = self.second_form_class(
            request.POST, request.FILES, instance=administrador)
        permisos = request.POST.getlist('permisos')
        if form.is_valid() and form2.is_valid() and permisos:
            usuario = form.save()
            form2.save()
            Permisos.objects.filter(user=usuario.id).delete()
            for pe in permisos:
                p = Permisos(user=usuario, permiso=int(pe))
                p.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            if not permisos:
                messages.add_message(request, messages.INFO,
                                     _('Seleccione los permisos.'))
            return self.render_to_response(
                self.get_context_data(
                    form=form, form2=form2))


@csrf_exempt
def CargarPermisos(request):
    if request.is_ajax():
        permisos = Permisos.objects.filter(
            user=request.POST['id'])
        object_list = serializers.serialize('json', permisos)
        return HttpResponse(object_list, content_type="application/json")

    return HttpResponseBadRequest()


class EliminarUser(DeleteView):
    model = User
    template_name = 'dashboard/eliminar_user.html'
    success_url = reverse_lazy('listar_user')


"""Vistas CRUD Usuarios Clientes"""
class ListarClientes(ListView):
    model = User
    template_name = 'dashboard/listar_clientes.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = User.objects.filter(is_staff=False)
        return queryset


class CrearCliente(CreateView):
    model = User
    form_class = UserForm
    template_name = 'dashboard/crear_cliente.html'
    success_url = reverse_lazy('listar_clientes')
    second_form_class = ClienteForm

    def get_context_data(self, **kwargs):
        context = super(CrearCliente, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST, request.FILES or None)
        if form.is_valid() and form2.is_valid():
            usuario = form.save(commit=False)
            usuario.is_staff = False
            usuario.save()
            cliente = form2.save(commit=False)
            cliente.user = usuario
            cliente.key_expires=datetime.datetime.now()
            cliente.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(
                    form=form, form2=form2))


class ModificarCliente(UpdateView):
    model = User
    second_model = Usuario
    form_class = UserFormEdit
    second_form_class = ClienteFormEdit
    template_name = 'dashboard/modif_cliente.html'
    success_url = reverse_lazy('listar_clientes')

    def get_context_data(self, **kwargs):
        context = super(ModificarCliente, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        usuario = self.model.objects.get(id=pk)
        cliente = self.second_model.objects.get(
            user=usuario.id)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=cliente)
            context['id'] = pk
            img = Usuario.objects.get(user=pk)
            context['imagen'] = u"/media/" + str(img.imagen)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id = kwargs['pk']
        usuario = self.model.objects.get(id=id)
        cliente = self.second_model.objects.get(
            user=usuario.id)
        form = self.form_class(request.POST, instance=usuario)
        form2 = self.second_form_class(
            request.POST, request.FILES, instance=cliente)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(
                    form=form, form2=form2))


def EstadoCliente(request, id):
    usuario = User.objects.get(id=id)
    if request.method == 'POST':
        if usuario.is_active:
            usuario.is_active = False
        else:
            usuario.is_active = True
        usuario.save()
        return redirect('listar_clientes')
    return render(request, 'dashboard/estado_cliente.html', {'usuario': usuario})


@csrf_exempt
def BuscarCliente(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    buscado = request.POST['buscado']
    palabras = buscado.split('%20')
    concat = None
    for palabra in palabras:
        if concat is None:
            concat = Q(first_name__icontains=palabra)
        else:
            concat = concat & Q(first_name__icontains=palabra)

    lista = User.objects.filter(concat).exclude(is_staff=True)
    dic = {}
    for i in lista:
        dic.update(
                        {i.id:{
                            'first_name':i.first_name,
                            'last_name':i.last_name,
                            'email':i.email,
                            'username':i.username,
                            'is_active':i.is_active,
                            'imagen':str(Usuario.objects.get(user=i.id).imagen),
                            }
                        })
    object_list=json.dumps(dic)

    return HttpResponse(object_list, content_type="application/json")


"""Vistas CRUD Configuracion de Valores Basicos"""
class CrearConfiguracion(CreateView):
    model = Configuracion
    form_class = ConfiguracionForm
    template_name = 'dashboard/configuracion.html'
    success_url = reverse_lazy('dashboard')


class EditarConfiguracion(UpdateView):
    model = Configuracion
    form_class = ConfiguracionForm
    template_name = 'dashboard/configuracion.html'
    success_url = reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = super(EditarConfiguracion, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        conf = self.model.objects.get(id_configuracion=pk)
        if 'form' not in context:
            context['form'] = self.form_class()
            context['id'] = pk
        img = Configuracion.objects.get(id_configuracion=pk)
        context['imagen'] = u"/media/" + str(img.imagen1nosotros)
        context['imagen2'] = u"/media/" + str(img.imagen2nosotros)
        context['imagen3'] = u"/media/" + str(img.imagen1info)
        context['imagen4'] = u"/media/" + str(img.imagen2info)
        context['imagen5'] = u"/media/" + str(img.imagen3info)
        return context


"""Vistas CRUD Marcas"""
class ListarMarcas(ListView):
    model = Marca
    template_name = 'dashboard/listar_marcas.html'
    paginate_by = 20


class CrearMarcas(CreateView):
    model = Marca
    form_class = MarcaForm
    template_name = 'dashboard/form_marcas.html'
    success_url = reverse_lazy('listar_marcas')


class ModificarMarcas(UpdateView):
    model = Marca
    form_class = MarcaForm
    template_name = 'dashboard/form_marcas.html'
    success_url = reverse_lazy('listar_marcas')


class EliminarMarcas(DeleteView):
    model = Marca
    template_name = 'dashboard/eliminar_marcas.html'
    success_url = reverse_lazy('listar_marcas')


@csrf_exempt
def cargar_marcas(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    al = Marca.objects.all()

    al_fields = ('nombre', )

    data = serializers.serialize('json', al, fields=al_fields)
    return HttpResponse(data, content_type="application/json")


"""Vistas CRUD Modelos"""
class ListarModelos(ListView):
    model = Modelo
    template_name = 'dashboard/listar_modelos.html'
    paginate_by = 20


class CrearModelos(CreateView):
    model = Modelo
    form_class = ModeloForm
    template_name = 'dashboard/form_modelos.html'
    success_url = reverse_lazy('listar_modelos')


class ModificarModelos(UpdateView):
    model = Modelo
    form_class = ModeloForm
    template_name = 'dashboard/form_modelos.html'
    success_url = reverse_lazy('listar_modelos')


class EliminarModelos(DeleteView):
    model = Modelo
    template_name = 'dashboard/eliminar_modelos.html'
    success_url = reverse_lazy('listar_modelos')


"""Vistas CRUD Testimoniales"""
class ListarTestimoniales(ListView):
    model = Testimoniales
    template_name = 'dashboard/listar_testimoniales.html'
    paginate_by = 20


class CrearTestimonial(CreateView):
    model = Testimoniales
    form_class = TestimonialesForm
    template_name = 'dashboard/form_testimoniales.html'
    success_url = reverse_lazy('listar_test')


class ModificarTestimonial(UpdateView):
    model = Testimoniales
    form_class = TestimonialesForm
    template_name = 'dashboard/form_testimoniales.html'
    success_url = reverse_lazy('listar_test')


def EstadoTestimonial(request, id):
    test = Testimoniales.objects.get(id_testimonial=id)
    if request.method == 'POST':
        if test.estado:
            test.estado = False
        else:
            test.estado = True
        test.save()
        return redirect('listar_test')
    return render(request, 'dashboard/estado_testimoniales.html', {'test': test})


class EliminarTestimonial(DeleteView):
    model = Testimoniales
    template_name = 'dashboard/eliminar_testimoniales.html'
    success_url = reverse_lazy('listar_test')


"""Vistas CRUD Servicios"""
class ListarServicios(ListView):
    model = Servicios
    template_name = 'dashboard/listar_servicios.html'


class CrearServicios(CreateView):
    model = Servicios
    form_class = ServiciosForm
    template_name = 'dashboard/crear_servicio.html'
    success_url = reverse_lazy('listar_serv')


class EditarServicios(UpdateView):
    model = Servicios
    form_class = ServiciosForm
    template_name = 'dashboard/modificar_servicio.html'
    success_url = reverse_lazy('listarserv')

    def get_context_data(self, **kwargs):
        context = super(EditarServicios, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        serv = self.model.objects.get(id_servicio=pk)
        if 'form' not in context:
            context['form'] = self.form_class()
            context['id'] = pk
        img = Servicios.objects.get(id_servicio=pk)
        context['imagene'] = u"/media/" + str(img.encabezado)
        context['imagend'] = u"/media/" + str(img.imagen_detalle)
        return contex


class EliminarServicios(DeleteView):
    model = Servicios
    template_name = 'dashboard/eliminar_servicios.html'
    success_url = reverse_lazy('listar_serv')


"""Vistas CRUD Noticias"""
class ListarNoticias(ListView):
    model = Noticias
    template_name = 'dashboard/listar_noticias.html'
    paginate_by = 20


class CrearNoticias(CreateView):
    model = Noticias
    form_class = NoticiasForm
    template_name = 'dashboard/crear_noticia.html'
    success_url = reverse_lazy('listar_noti')


class EditarNoticias(UpdateView):
    model = Noticias
    form_class = NoticiasForm
    template_name = 'dashboard/modificar_noticia.html'
    success_url = reverse_lazy('listar_noti')

    def get_context_data(self, **kwargs):
        context = super(EditarNoticias, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        noti = self.model.objects.get(id_noticia=pk)
        if 'form' not in context:
            context['form'] = self.form_class()
            context['id'] = pk
        img = Noticias.objects.get(id_noticia=pk)
        context['imagen'] = u"/media/" + str(img.imagen)
        return context


class EliminarNoticias(DeleteView):
    model = Noticias
    template_name = 'dashboard/eliminar_noticias.html'
    success_url = reverse_lazy('listar_noti')


def EstadoNoticias(request, id):
    noti = Noticias.objects.get(id_noticia=id)
    if request.method == 'POST':
        if noti.estado:
            noti.estado = False
        else:
            noti.estado = True
        noti.save()
        return redirect('listar_noti')
    return render(request, 'dashboard/estado_noticias.html', {'noticia': noti})


"""Vistas de Mensajeria"""
class ListarMensajes(ListView):
    model = Mensajeria
    template_name = 'dashboard/listar_mensajes.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = Mensajeria.objects.filter(respuesta="")
        return queryset


def VerMensaje(request,id):
    mensaje=Mensajeria.objects.get(id_mensaje=id)
    mensaje.estado=True
    mensaje.save()
    return render(request, "dashboard/responder_mensaje.html", {'mensaje':mensaje})


@csrf_exempt
def enviar_respuesta(request):
    if request.is_ajax():
        mensaje = Mensajeria.objects.get(id_mensaje=request.POST['id'])
        usuario= User.objects.get(id=int(request.POST['usuario']))
        mensaje.respuesta=request.POST['respuesta']
        mensaje.usuario_reponde=usuario
        mensaje.save()
        #enviar email
        print(mensaje.correo)
        email_subject = 'Respuesta de Contacto'
        email_body = "Hola %s, \n %s. \n Gracias por contactarnos." % (mensaje.nombre,request.POST['respuesta'])
        send_mail(email_subject, email_body, mensaje.correo,
                [mensaje.correo], fail_silently=False)
        return HttpResponse(json.dumps({'mensaje': 'Respuesta enviada correctamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()


"""Vistas de Logos de Clientes"""
def Logos(request):
    form = LogosClientesForm(request.POST or None,request.FILES or None)
    logos = LogosClientes.objects.all()
    if not request.is_ajax() and request.method == 'POST':
        if form.is_valid():
            form.save()
    #if request.is_ajax():

    return render(request, "dashboard/logos.html", {'form':form, 'logos':logos})


@csrf_exempt
def LogosEliminar(request, id):
    if request.is_ajax():
        logo = LogosClientes.objects.get(id_logo=id).delete()

        return HttpResponse(json.dumps({'mensaje': 'Logo eliminado correctamente.'}), content_type="application/json")
    else:
        return HttpResponseBadRequest()