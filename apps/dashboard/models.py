from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Configuracion(models.Model):
    id_configuracion = models.AutoField(primary_key=True)
    precio_usd = models.DecimalField(max_digits=19, decimal_places=3, default=0.0,
                                     null=False, blank=False, verbose_name=_('precio del dolar en bs.'))
    pago_bs=models.BooleanField(default=False,verbose_name=_('permite precio en bs.'))
    imagen1nosotros=models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen nosotros 1'))
    imagen2nosotros=models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen nosotros 2'))
    descripcion_nosotros=models.TextField(null=False, blank=False, default="", verbose_name=_('descripción nosotros'))
    imagen1info=models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen informacion 1'))
    imagen2info=models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen informacion 2'))
    imagen3info=models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen informacion 3'))
    descripcion_info=models.TextField(null=False, blank=False, default="", verbose_name=_('descripción informacion'))

    class Meta:
        db_table = 'configuracion'
        verbose_name = 'configuracion'


class Administradores(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    pais = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('pais de residencia'))
    ciudad = models.CharField(max_length=50, null=False,
                           blank=True, verbose_name=_('ciudad de residencia'))
    imagen = models.ImageField(upload_to='profile_photo/', default="",null=False,
                               blank=True, verbose_name=_('imagen del administrador'))
    class Meta:
        db_table = 'administradores'
        verbose_name = 'administradores'

class Permisos(models.Model):
    user = models.ForeignKey(User, null=False, blank=False, verbose_name=_('usuario relacionado'))
    permiso = models.IntegerField(null=False, blank=False, default=0, verbose_name=_('permiso asignado'))

    class Meta:
        db_table = 'permisos'
        verbose_name = 'permisos'

class Servicios(models.Model):
    id_servicio = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=50, null=False, blank=False, default=0, verbose_name=_('tipo de servicio'))
    tipo=models.CharField(max_length=5, null=False, blank=False, verbose_name=_('Nombre del servicio'))
    encabezado = models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen encabezado'))
    imagen_detalle = models.ImageField(upload_to='encabezados/', default="",null=False,
                               blank=True, verbose_name=_('imagen detalle'))
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('descripción del servicio'))
    descuento = models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('Descuento en el Servicio'))
    maneja_seguro = models.BooleanField(blank=False, default=False, verbose_name=_('Servicio maneja Seguro'))
    estado = models.BooleanField(blank=False, default=True, verbose_name=_('Estado del Servicio'))
    divisor_pg = models.IntegerField(
        default=0, null=False, blank=False, verbose_name=_('divisor de peso en pulgadas'))
    divisor_cm = models.IntegerField(
        default=0, null=False, blank=False, verbose_name=_('divisor de peso en centimetros'))
    precio_lb = models.DecimalField(max_digits=19, decimal_places=3, default=0.0,
                                    null=False, blank=False, verbose_name=_('precio por libra'))
    precio_kg = models.DecimalField(max_digits=19, decimal_places=3, default=0.0,
                                    null=False, blank=False, verbose_name=_('precio por Kilogramo'))

    class Meta:
        db_table = 'servicios'
        verbose_name = 'servicio'


class LogosClientes(models.Model):
    id_logo = models.AutoField(primary_key=True)
    imagen = models.ImageField(upload_to='logos_clientes/', default="",null=False,
                               blank=True, verbose_name=_('imagen logo'))

    class Meta:
        db_table = 'logos_clientes'
        verbose_name = 'logos clientes'


class SuperCategorias(models.Model):
    id_super = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False, verbose_name=_('nombre de la super categoria'))
    
    class Meta:
        db_table = 'super_categoria'
        verbose_name = 'super categoría'


class Categorias(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False, verbose_name=_('nombre de la categoria'))
    id_super = models.ForeignKey(SuperCategorias, null=True, blank=True, verbose_name=_('super categoría'))
    
    class Meta:
        db_table = 'categoria'
        verbose_name = 'categoría'


class SubCategorias(models.Model):
    id_subcategoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False, verbose_name=_('nombre de la subcategoria'))
    id_categoria = models.ForeignKey(Categorias, null=True, blank=True, verbose_name=_('categoría'))
    
    class Meta:
        db_table = 'subcategoria'
        verbose_name = 'sub categoría'


class Marca(models.Model):
    id_marca = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50,null=False,blank=False,verbose_name=_('nombre de la marca'))

    class Meta:
        db_table = 'marca'
        verbose_name = 'marca'

    def __str__(self):
        return self.nombre


class Modelo(models.Model):
    id_modelo = models.AutoField(primary_key=True)
    id_marca = models.ForeignKey(Marca, null=True, blank=True, verbose_name=_('marca relacionada'))
    nombre = models.CharField(max_length=50,null=False,blank=False,verbose_name=_('nombre del modelo'))

    class Meta:
        db_table = 'modelo'
        verbose_name = 'modelo'


class Productos(models.Model):
    id_producto = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=250, null=False, blank=False, verbose_name=_('descripción'))
    id_subcategoria = models.ForeignKey(SubCategorias,null=False,blank=False,default=0,verbose_name=_('categoría relacionada'))
    id_marca = models.ForeignKey(Marca,null=False,blank=False,default=0,verbose_name=_('marca del producto'))
    id_modelo = models.ForeignKey(Modelo,null=False,blank=False,default=0,verbose_name=_('modelo del producto'))
    url_video = models.FileField(upload_to='videos/',null=False,blank=False,verbose_name=_('video descriptivo'))
    fecha_publicacion = models.DateField(auto_now_add=True, editable=True, verbose_name=_('fecha de publicacion del producto'))

    class Meta:
        db_table = 'producto'
        verbose_name = 'producto'


class DetalleProductos(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_producto = models.ForeignKey(Productos,null=False,blank=False,default=0,verbose_name=_('producto relacionado'))
    color = models.CharField(max_length=10, null=False, blank=False, verbose_name=_('color del producto'))
    precio = models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('precio del producto'))
    url_imagen = models.ImageField(upload_to='fotos_productos/',null=False,blank=False,verbose_name=_('video descriptivo'))
    existencia = models.IntegerField(default=0, null=False, blank=False, verbose_name=_('existencia del producto'))
    estado = models.BooleanField(blank=False, default=True, verbose_name=_('estado del producto'))
    
    class Meta:
        db_table = 'detalleproducto'
        verbose_name = 'detalle del producto'


class DireccionFacturacionUsuario(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    pais = models.CharField(max_length=50, null=True,
                            blank=True, default='', verbose_name=_('país'))
    estado = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('estado'))
    ciudad = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('ciudad'))
    calle = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('calle'))
    codigo_postal = models.CharField(
        max_length=10, null=True, blank=True, default='', verbose_name=_('codigo postal'))
    telefono = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('telefono'))
    telefono2 = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('telefono 2s'))

    class Meta:
        db_table = 'direccion_facturacion'
        verbose_name = 'direccion de usuario'


class DireccionEnvioUsuario(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario relacionado'))
    paise = models.CharField(max_length=50, null=True,
                            blank=True, default='', verbose_name=_('país'))
    estadoe = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('estado'))
    ciudade = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('ciudad'))
    callee = models.CharField(max_length=50, null=True,
                              blank=True, default='', verbose_name=_('calle'))
    codigo_postale = models.CharField(
        max_length=10, null=True, blank=True, default='', verbose_name=_('codigo postal'))
    telefonoe= models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('telefono'))
    telefonoe2 = models.CharField(
        max_length=50, null=False, blank=True, verbose_name=_('telefono 2s'))

    class Meta:
        db_table = 'direccion_envio'
        verbose_name = 'direccion de usuario'


class Envios(models.Model):
    id_envio = models.AutoField(primary_key=True)
    id_usuario = models.ForeignKey(User,null=False,blank=False,default=0,verbose_name=_('usuario comprador'))
    id_direccion = models.ForeignKey(DireccionEnvioUsuario,null=False,blank=False,verbose_name=_('direccion de envio'))
    id_servicio = models.ForeignKey(Servicios,null=False,blank=False,default=0,verbose_name=_('servicio contratado'))
    monto_dec = models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('monto declarado'))
    moneda_dec = models.CharField(max_length=5, null=False, blank=False, verbose_name=_('moneda de declaracion'))
    cantidad_piezas = models.IntegerField(default=0, null=False, blank=False, verbose_name=_('cantidad de piezas en el envio'))
    peso = models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('peso'))
    unidad_peso = models.CharField(max_length=5, null=False, blank=False, verbose_name=_('unidad de peso'))
    alto= models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('alto'))
    largo= models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('largo'))
    ancho= models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('ancho'))
    unidad_medida = models.CharField(max_length=5, null=False, blank=False, verbose_name=_('unidad de medida'))
    nro_tracking = models.CharField(max_length=20, null=False, blank=False, verbose_name=_('nro de tracking'))
    total_envio= models.DecimalField(max_digits=19,decimal_places=3,default=0.0, null=False, blank=False, verbose_name=_('monto total del envio'))
    moneda_envio = models.CharField(max_length=5, null=False, blank=False, verbose_name=_('moneda de pago del envio'))
    fecha = models.DateField(auto_now_add=True, editable=True, verbose_name=_('fecha de envio'))
    estado = models.CharField(max_length=20, null=False, blank=False, verbose_name=_('estado del envio'))

    class Meta:
        db_table = 'envio'
        verbose_name = 'envio'


class DetalleEnvio(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_envio = models.ForeignKey(Envios,null=False,blank=False,default=0,verbose_name=_('envio relacionado'))
    id_producto = models.ForeignKey(Productos,null=False,blank=False,default=0,verbose_name=_('producto relacionado'))
    cantidad = models.IntegerField(default=0, null=False, blank=False, verbose_name=_('cantidad enviada'))
    
    class Meta:
        db_table = 'detalle_envio'
        verbose_name = 'detalle del envio'


class Mensajeria(models.Model):
    id_mensaje = models.AutoField(primary_key=True)
    usuario_reponde = models.ForeignKey(User,null=True,blank=True,verbose_name=_('usuario que responde'))
    nombre = models.CharField(max_length=80, null=False, blank=False, default="", verbose_name=_('nombre'))
    apellido = models.CharField(max_length=50, null=False, blank=False, default="", verbose_name=_('apellido'))
    correo = models.CharField(max_length=250,null=False, blank=False, default="", verbose_name=_('correo electronico'))
    mensaje = models.TextField(null=False, blank=False, default="", verbose_name=_('mensaje'))
    fecha_recibido = models.DateField(auto_now_add=True, editable=False, null=True, verbose_name=_('fecha del mensaje'))
    respuesta = models.TextField(null=False, blank=True, default="", verbose_name=_('mensaje de respuesta'))
    estado = models.BooleanField(blank=False, default=False, verbose_name=_('estado del mensaje'))
    
    class Meta:
        db_table = 'mensajeria'
        verbose_name = 'mensajeria'
